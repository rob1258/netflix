package k;

public abstract class contenido {

	// atributos

	private String nombre;
	private int id;
	private String fecha;
	private int valoracion;
	private boolean visto;

	// constructor

	public contenido() {
		this.nombre = "no definido";
		this.id = 0;
		this.fecha = "no definida";
		this.visto = false;
	}

	// constructor todo

	public contenido(String nombre, int id, String fecha, int valoracion, boolean visto) {
		this.nombre = nombre;
		this.id = id;
		this.fecha = fecha;
		this.valoracion = valoracion;
		this.visto = visto;
	}

	// constructor sin val

	public contenido(String nombre, int id, String fecha, boolean visto) {
		this.nombre = nombre;
		this.id = id;
		this.fecha = fecha;
		this.visto = visto;
	}

	// get set

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean getVisto() {
		return visto;
	}

	public void setVisto(boolean visto) {
		this.visto = visto;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getValoracion() {
		return valoracion;
	}

	public void setValoracion(int valoracion) {
		this.valoracion = valoracion;
	}

	public void valoracion(int valoracion) {
		setValoracion(valoracion);
	}

	public abstract void corregir(String correct, String selector);

	// corr

	@Override
	public String toString() {
		return nombre + ": " + "\n\t- Fecha Inicio: " + fecha + "\n\t- Valoracion:" + valoracion + "\n\t- ID: " + id
				+ "\n\t- �Visto?:" + visto;
	}

}
