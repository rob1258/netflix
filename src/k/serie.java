package k;

public class serie extends contenido {

	private String estudio;
	private int n_temporadas;
	private int estreno;
	private int[] valoracion_temporadas;

	// Constructor default

	public serie() {

	}

	// constructor val

	public serie(String nombre, int id, String fecha, int valoracion, boolean visto, String estudio, int n_temporadas,
			int estreno) {
		super(nombre, id, fecha, valoracion, visto);
		this.estudio = estudio;
		this.n_temporadas = n_temporadas;
		this.estreno = estreno;
	}

	// constructor sin val

	public serie(String nombre, int id, String fecha, boolean visto, String estudio, int n_temporadas, int estreno) {
		super(nombre, id, fecha, visto);
		this.estudio = estudio;
		this.n_temporadas = n_temporadas;
		this.valoracion_temporadas = new int[this.n_temporadas];
		this.estreno = estreno;
	}

	// get set

	public String getEstudio() {
		return estudio;
	}

	public void setEstudio(String estudio) {
		this.estudio = estudio;
	}

	public int getN_temporadas() {
		return n_temporadas;
	}

	public void setN_temporadas(int n_temporadas) {
		this.n_temporadas = n_temporadas;
	}

	public int getEstreno() {
		return estreno;
	}

	public void setEstreno(int estreno) {
		this.estreno = estreno;
	}

	public int[] getValoracion_temporadas() {
		return valoracion_temporadas;
	}

	public void setValoracion_temporadas(int[] valoracion_temporadas) {
		this.valoracion_temporadas = valoracion_temporadas;
	}

	public void valoracionTemporadas(int[] notas) {
		if (notas.length == this.n_temporadas) {
			this.setValoracion_temporadas(notas);
		} else {
			System.out.println("Problema en el numero de valoraciones");
		}
	}

	// corr

	@Override
	public String toString() {
		return super.toString() + "\n\t- Numero de temporadas: " + n_temporadas + "\n\t- Estreno: " + estreno
				+ "\n\t- Estudio: " + estudio;
	}

	@Override
	public void corregir(String correct, String selector) {
		switch (selector) {
		case "1":
			setNombre(correct);
			break;

		case "2":
			setFecha(correct);
			break;

		case "3":
			setEstudio(correct);
			break;

		case "4":
			setN_temporadas(Integer.parseInt(correct));
			break;

		case "5":
			setEstreno(Integer.parseInt(correct));
			break;

		case "6":
			setVisto(Boolean.parseBoolean((correct)));
			break;

		case "7":
			setId(Integer.parseInt(correct));
			break;

		default:
			System.out.println("falta");
			break;

		}
	}

}
