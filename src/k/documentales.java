package k;

public class documentales extends contenido {

	private String director;
	private int a�o;

	// Constructor default

	public documentales() {

	}

	// constructor todo

	public documentales(String nombre, int id, String fecha, int valoracion, boolean visto, String director, int a�o) {
		super(nombre, id, fecha, valoracion, visto);
		this.director = director;
		this.a�o = a�o;
	}

	// constructor sin val

	public documentales(String nombre, int id, String fecha, boolean visto, String director, int a�o) {
		super(nombre, id, fecha, visto);
		this.director = director;
		this.a�o = a�o;
	}

	// get set

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getA�o() {
		return a�o;
	}

	public void setA�o(int a�o) {
		this.a�o = a�o;
	}

	// corr

	@Override
	public String toString() {
		return super.toString() + "\n\t- Director/a: " + director + "\n\t- A�o: " + a�o;
	}

	@Override
	public void corregir(String correct, String selector) {
		switch (selector) {
		case "1":
			setNombre(correct);
			break;

		case "2":
			setFecha(correct);
			break;

		case "3":
			setDirector(correct);
			break;

		case "4":
			setA�o(Integer.parseInt(correct));
			break;

		case "5":
			setVisto(Boolean.parseBoolean((correct)));
			break;

		case "6":
			setId(Integer.parseInt(correct));
			break;

		default:
			System.out.println("falta");
			break;

		}
	}
}
