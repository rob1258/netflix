package k;

import java.util.ArrayList;

public class j {

	public static usuario usuario1 = new usuario("Roberto");

	public static void main(String[] args) {

		// 1
		System.out.println("#1 A�adir The clockwork, The Blue Planet y Stranger Things:");
		usuario1.addContenidos(new pelicula("The clockwork orange", 1, "24/11/2021", true, "Cubrick", 1971));
		usuario1.addContenidos(new documentales("The Blue Planet", 2, "24/11/2021", true, "Alastair Fothergill", 2001));
		usuario1.addContenidos(new serie("Stranger Things", 3, "10/09/2021", true, "Netflix", 3, 2016));
		imprimirRegistroCompletoContenido();

		// 2
		System.out.println("#2 Valorar Stranger Things:");
		usuario1.getContenidos().get(2).valoracion(7);
		if (usuario1.getContenidos().get(2) instanceof serie) {
			int[] valoracion_temporadas = { 9, 0, 9 };
			((serie) usuario1.getContenidos().get(2)).valoracionTemporadas(valoracion_temporadas);
		}
		System.out.println(usuario1.getContenidos().get(2));

		// 3
		System.out.println("#3 Corregir Cubrick a Kubrick:");
		usuario1.getContenidos().get(0).corregir("Kubrick", "3");
		System.out.println(usuario1.getContenidos().get(0));

		// 4
		System.out.println("#4 Hermana mira The Notebook");
		usuario1.addContenidos(new pelicula("The Notebook", 4, "11/09/2021", true, "Nick Cassavetes", 2004));
		System.out.println(usuario1.getContenidos().get(3));

		// 5
		System.out.println("#5 Borrar the Notebook y valorar peliculas");
		usuario1.deleteContenidos(3);
		usuario1.getContenidos().get(0).valoracion(8);
		usuario1.getContenidos().get(1).valoracion(6);
		imprimirRegistroCompletoContenido();

		// 6
		System.out.println("#6 A�adir The Shining y Pet Sematary con valoraci�n");
		usuario1.addContenidos(new pelicula("The Shining", 5, "13/09/2021", 9, true, "Stephen King", 1980));
		usuario1.addContenidos(new pelicula("Pet Sematary", 6, "13/09/2021", 8, true, "Stephen King", 1989));
		imprimirRegistroCompletoContenido();

		// 7
		System.out.println("#7 Filtrar peliculas por su valoracion");
		imprimirRegistroFiltrado("1");

	}

	private static void imprimirRegistroCompletoContenido() {
		imprimirRegistroCompletoContenido(usuario1.getContenidos());
	}

	private static void imprimirRegistroCompletoContenido(ArrayList<contenido> contenido) {
		for (int i = 0; i < contenido.size(); i++) {
			System.out.println(i + "- " + contenido.get(i));
		}
		System.out.println();
	}

	private static void imprimirRegistroFiltrado(String opcion) {
		ArrayList<contenido> contenidosFiltro = new ArrayList<>();
		switch (opcion) {
		case "1":
			for (int i = 0; i < usuario1.getContenidos().size(); i++) {
				if (usuario1.getContenidos().get(i) instanceof pelicula) {
					contenidosFiltro.add(usuario1.getContenidos().get(i));
				}
			}
			break;
		case "2":
			for (int i = 0; i < usuario1.getContenidos().size(); i++) {
				if (usuario1.getContenidos().get(i) instanceof serie) {
					contenidosFiltro.add(usuario1.getContenidos().get(i));
				}
			}
			break;
		case "3":
			for (int i = 0; i < usuario1.getContenidos().size(); i++) {
				if (usuario1.getContenidos().get(i) instanceof documentales) {
					contenidosFiltro.add(usuario1.getContenidos().get(i));
				}
			}
			break;

		case "4":
			contenidosFiltro = usuario1.getContenidos();
			break;

		default:
			break;
		}

		contenidosFiltro.sort((o1, o2) -> {
			if (o1.getValoracion() > o2.getValoracion()) {
				return -1;
			} else if (o1.getValoracion() == o2.getValoracion()) {
				return 0;
			} else {
				return 1;
			}
		});

		imprimirRegistroCompletoContenido(contenidosFiltro);
	}

}
