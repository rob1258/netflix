package k;

import java.util.ArrayList;

public class usuario {

	private String nombre;
	private ArrayList<contenido> contenidos;

	public usuario(String nombre) {
		this.nombre = nombre;
		this.contenidos = new ArrayList<contenido>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<contenido> getContenidos() {
		return contenidos;
	}

	public void setContenidos(ArrayList<contenido> contenidos) {
		this.contenidos = contenidos;
	}

	public void addContenidos(contenido contenidos) {
		this.contenidos.add(contenidos);
	}

	public void deleteContenidos(int posicion) {
		this.contenidos.remove(posicion);
	}
}
